public class DN04 {
	
	private int n1;
	private int n2;
	private int n5;
	private int najvecjiDvig = 0;
	private int najvecNaDan = 0;
	private int trenutnoNaDan = 0;
	private Datum bestDatum = null;
	private Datum prejsniDatum = null;

	public Bankomat() {
		this.n1 = 0;
		this.n2 = 0;
		this.n5 = 0;
	}
	
	public int vrniN5() {
		return n5;
	}
	
	public int vrniN2() {
		return n2;
	}
	
	public int vrniN1() {
		return n1;
	}
	
	public void nalozi(int k5, int k2, int k1) {
		this.n1 += k1;
		this.n2 += k2;
		this.n5 += k5;
	}
	
	public void izpisi() {
		System.out.printf("%d | %d | %d%n", n5, n2, n1);
	}
	
	public int kolicinaDenarja() {
		return (5 * n5) + (2 * n2) + n1;
	}
	
	public boolean dvigni(int dvig, Datum datum) {
		int ostanek = 0;
		int t5 = 0;
		int t2 = 0;
		int t1 = 0;

		if (kolicinaDenarja() < dvig) {
			return false;
			
		}else {
			ostanek = dvig / 5;
			if (n5 < ostanek) {
				t5 = n5;
			}else {
				t5 = ostanek;
			}
			
			ostanek = dvig - (t5 * 5);
			if ((n1 == 0) && (ostanek % 2 != 0)) {
				t5--;
				//System.out.println("PAZI!!!!");
			}
			
			ostanek = dvig - (t5 * 5);
			//System.out.println(ostanek + " + " + t5);
			if (n2 < ostanek / 2) {
				t2 = n2;
			}else {
				t2 = ostanek / 2;
			}
			
			ostanek -= t2 * 2;
			if (n1 < ostanek) {
				t1 = n1;
			}else {
				t1 = ostanek;
			}
			
			ostanek = (t5 * 5) + (t2 * 2) + t1;
			
			if (ostanek == dvig) {
					n5 -= t5;
					n2 -= t2;
					n1 -= t1;
					
				if (prejsniDatum != null) {
					if (datum.jeEnakKot(prejsniDatum)) {
						trenutnoNaDan += dvig;
						//System.out.println(trenutnoNaDan);
					}else {
						prejsniDatum = datum;
						trenutnoNaDan = dvig;
						
					}
					
					if (trenutnoNaDan > najvecNaDan) { // more skos prpeverjat, drugace,ce je nakoncu datum najvecji nikoli ne pride v else....!!!
						bestDatum = datum;
						najvecNaDan = trenutnoNaDan;
					}
					
				}else {
					prejsniDatum = datum;
					bestDatum = datum;
					trenutnoNaDan = dvig;
					najvecNaDan = trenutnoNaDan;
				}
					
				if (najvecjiDvig < dvig) {
					najvecjiDvig = dvig;
				}
				
				return true;
				
			}else {
				
				return false;
			}
		}
	}
	
	public int najDvig() {
		return najvecjiDvig;
	}
	
	public Datum najDatum() {
		return bestDatum;		
	}
	
}