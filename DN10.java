
abstract class DN10 {
	
	protected Izraz levi;
	protected Izraz desni;
	protected char operator;
	protected static int stOperatorjev = 0;
	
	public static Izraz zgradi(String niz) {
		//najprej se znebimo vseh nepotrebnih oklepajev
		String skrajsanNiz = odstrani(niz);
		
		//ce imamo niz dolg 1 pomeni da je to samo stevilka!
		if (skrajsanNiz.length() == 1) {
			return new Stevilo(Integer.parseInt(skrajsanNiz));
		}
		
		String levi = "";
		String desni = "";
		char op = '+';
		boolean niPlusovMinusov = true;
		int stejOklepaje = 0;
		
		for (int k = skrajsanNiz.length() - 1; k >= 0; k--) {
			if (skrajsanNiz.charAt(k) == '(') {
				stejOklepaje++;
				
			}else if (skrajsanNiz.charAt(k) == ')') {
				stejOklepaje--;
			}
			
			if (stejOklepaje == 0 && (skrajsanNiz.charAt(k) == '+' || skrajsanNiz.charAt(k) == '-')) {
				levi = skrajsanNiz.substring(0, k);
				desni = skrajsanNiz.substring(k + 1, skrajsanNiz.length());
				op = skrajsanNiz.charAt(k);
				stOperatorjev++;
				niPlusovMinusov = false;
				break;
			}
		}
		
		if (niPlusovMinusov) {
			for (int k = skrajsanNiz.length() - 1; k >= 0; k--) {
				if (skrajsanNiz.charAt(k) == '(') {
					stejOklepaje++;
					
				}else if (skrajsanNiz.charAt(k) == ')') {
					stejOklepaje--;
				}
				
				if (stejOklepaje == 0 && (skrajsanNiz.charAt(k) == '/' || skrajsanNiz.charAt(k) == '*')) {
					levi = skrajsanNiz.substring(0, k);
					desni = skrajsanNiz.substring(k + 1, skrajsanNiz.length());
					op = skrajsanNiz.charAt(k);
					stOperatorjev++;
					break;	
				}
			}
		}
		
		//System.out.println(levi + " # " + op + " # " + desni);
		return new SestavljenIzraz(zgradi(levi), op, zgradi(desni));
	}
	
	public static String odstrani(String niz) {
		while (true) {
			int stOklepajev = 0;

			if (niz.charAt(0) == '(' && niz.charAt(niz.length() - 1) == ')') { //niz.length() > 0 && 
				for (int i = 0; i < niz.length() - 1; i++) {
					if (niz.charAt(i) == '(') {
						stOklepajev++;
						
					}else if (niz.charAt(i) == ')') {
						stOklepajev--;
						
						if (stOklepajev == 0) { //&& i != niz.length() - 1
							return niz;
						}
					}
				}
				
				niz = niz.substring(1, niz.length() - 1);
			}else {
				break;
			}
		}

		return niz;
	}
	
	public int steviloOperatorjev() {
		return stOperatorjev;
	}
	
	public abstract String postfiksno();
	public abstract int vrednost();
	
}

class Stevilo extends Izraz {
	
	private int stevilo;
	
	public Stevilo(int stevilo) {
		this.stevilo = stevilo;
	}
	
	@Override
	public int vrednost() {
		return this.stevilo;
	}
	
	@Override
	public String postfiksno () {
		return String.valueOf(stevilo);
	}
	
}

class SestavljenIzraz extends Izraz {
	
	public SestavljenIzraz(Izraz levi, char operator, Izraz desni) {
		this.levi = levi;
		this.operator = operator;
		this.desni = desni;
	}
	
	@Override
	public int vrednost() {
		switch(this.operator) {
			case '+':
				return this.levi.vrednost() + this.desni.vrednost();
			case '-':
				return this.levi.vrednost() - this.desni.vrednost();
			case '/':
				return this.levi.vrednost() / this.desni.vrednost();
			case '*':
				return this.levi.vrednost() * this.desni.vrednost();
		}
		
		return 0;
	}
	
	@Override
	public String postfiksno() {
		return String.valueOf(this.levi.postfiksno()) 
		+ String.valueOf(this.desni.postfiksno())
		+ String.valueOf(this.operator);
	}
	
}






















































