import java.util.Scanner;

public class DN03 {
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		int meseci_v_letu = sc.nextInt();
		int dnevi_v_mesecu = sc.nextInt();
		int dnevi_v_tednu = sc.nextInt();
		int delaProstDan = sc.nextInt();
		int praznik = sc.nextInt();
		
		int zacMesec = sc.nextInt();
		int zacLeto = sc.nextInt();
		int konMesec = sc.nextInt();
		int konLeto = sc.nextInt();
		
		int stMesecev = (((konLeto - 1) * meseci_v_letu) + konMesec) - (((zacLeto - 1) * meseci_v_letu) + zacMesec); // koliko mesecev je vmes ...
		int dosedajsniDnevi = (((zacLeto - 1) * meseci_v_letu * dnevi_v_mesecu) + ((zacMesec - 1) * dnevi_v_mesecu));
		int zamik = dosedajsniDnevi % dnevi_v_tednu; // koliko je zamaknjen koledar (ker ne zacne vedno s ponedeljkom)
		
		int trenutnoLeto = zacLeto;
		int trenutniMesec = zacMesec;
		int trenutniDan = zamik + 1;
		
		for (int m = 0; m <= stMesecev; m++) {
			trenutniMesec %= meseci_v_letu;
			
			if (trenutniMesec == 0) {
				trenutniMesec = meseci_v_letu;
				System.out.println(trenutniMesec + "/" + trenutnoLeto);
				trenutnoLeto++;
				
			}else {
				System.out.println(trenutniMesec + "/" + trenutnoLeto);
				
			}
			
			trenutniMesec++;
			presledki(zamik);
			
			for (int dan = 1; dan <= dnevi_v_mesecu; dan++) {
				trenutniDan %= dnevi_v_tednu;
				
				System.out.printf("%4d", dan);
				
				if (trenutniDan % dnevi_v_tednu == 0) {
					trenutniDan = dnevi_v_tednu;
				}
				
				if ((dosedajsniDnevi + dan) % praznik == 0&& trenutniDan % delaProstDan == 0) {
					System.out.print("*");
					
				}else if ((dosedajsniDnevi + dan) % praznik == 0) {
					System.out.print("+");
					
				}else if (trenutniDan % delaProstDan == 0) {
					System.out.print("x");
					
				}else {
					System.out.print("_");
					
				}
				
				if ((dan + zamik) % dnevi_v_tednu == 0 || dan == dnevi_v_mesecu) {
					System.out.println();
				}
				
				trenutniDan++;
			}
			
			zamik = (zamik + dnevi_v_mesecu) % dnevi_v_tednu;	
			dosedajsniDnevi += dnevi_v_mesecu;
		}
	}
	
	public static void presledki(int i) {
		for (int k = 1; k <= i; k++) {
			System.out.print("     ");
		}
	}
	
}