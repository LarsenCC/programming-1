import java.util.Scanner;

public class DN05 {
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		int dolzina = sc.nextInt();
		int[] array = new int[dolzina];
	
		int predhodni = sc.nextInt();
		int max = predhodni;
		int iMax = 0;
		long sum = 1;
		array[0] = predhodni;
		
		for (int i = 1; i < dolzina; i++) {
			int trenutni = sc.nextInt();
			array[i] = trenutni;
			
			if (trenutni >= max) {
				sum += i + 1;
				iMax = i;
				max = trenutni;
				
			}else if (trenutni < predhodni) {
				sum++;
				
			}else if (trenutni >= predhodni){
				for (int k = i - 2; k >= iMax; k--) {
					if (array[k] > trenutni) {
						sum += (i - k);
						break;
					}
				}
			}
			
			predhodni = trenutni;
		}
		
		System.out.println(sum);
	}
}