import java.util.Scanner;

public class DN01 {
	
	public static void main (String[] args) {
		Scanner sc = new Scanner(System.in);
		
		int perioda = sc.nextInt();
		int valDolzina = sc.nextInt();
		int amplituda = sc.nextInt();
		int zamik = sc.nextInt();
		int zacetek = 1;
		
		zamik %= valDolzina; //za katerokoli periodo ...
		int dolzinaVrste = perioda*valDolzina + zamik;
		if (zamik >= valDolzina/2) {zacetek *= -1;}
		
		for (int k = 0; k <= amplituda; k++){
			if (k == amplituda) {zacetek *= -1;}
			for (int i = zamik + 1; i <= dolzinaVrste; i++) {
				
				if (i%(valDolzina/2) == 0) {
					if (k == 0 || k == amplituda) {
						System.out.printf("+");
						zacetek *= -1;
					}else {
						System.out.printf("|");
					}
					
				}else if (zacetek == 1 && (k == 0 || k == amplituda)){
					System.out.printf("-");
					
				}else {
					System.out.printf(" ");	
				}
			}
			System.out.printf("%n");
		}
	}
}