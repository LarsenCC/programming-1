import java.util.Scanner;

public class DN07 {
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		int postaje = sc.nextInt();
		int[] casPostaje = new int[postaje - 1];
		int vsotaCasovA = 0;
		int vsotaCasovB = 0;
		
		for (int i = 0; i < casPostaje.length; i++) {
			casPostaje[i] = sc.nextInt();

			if (casPostaje.length / 2 > i) {
				vsotaCasovA += casPostaje[i];
			}else {
				vsotaCasovB += casPostaje[i];
			}
		}

		int voznje = sc.nextInt();
		int sum = 0;
		
		for (int i = 0; i < voznje; i++) {
			int a = sc.nextInt();
			int b = sc.nextInt();
			int minimum = vsotaCasovA - vsotaCasovB + a - b; //zacetni minumum
			int trenutni = 0; // trenutni minimum
			int prejsni = 0;
			int k = (casPostaje.length / 2) - 1;
			boolean check = false;
			
			if (Math.abs(a - b) >= vsotaCasovA + vsotaCasovB || minimum == 0) {
				continue;
			}
			
			while (minimum > 0) {
				trenutni = minimum - casPostaje[k] * 2;
				if (minimum > trenutni) {
					prejsni = minimum;
					minimum = trenutni;
				}
				k--;
				check = true;
			}
			
			if (!check) {
				while (minimum < 0) {
					k++;
					trenutni = minimum + casPostaje[k] * 2;
					if (minimum < trenutni) {
						prejsni = minimum;
						minimum = trenutni;
					}
				}
			}
			
			if (Math.abs(trenutni) < Math.abs(prejsni)) {
				sum += Math.abs(trenutni);
			}else {
				sum += Math.abs(prejsni);
			}
			
		}
		
		System.out.println(sum);
	}
}