import java.util.Scanner;

public class DN06 {
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		int vrstice = sc.nextInt();
		int stolpci = sc.nextInt();
		int stDrzav = 0;
		int stCelicMorja = 0;
		
		int[][] zemljevid = new int[vrstice][stolpci];
		
		for (int v = 0; v < zemljevid.length; v++) {
			for (int s = 0; s < zemljevid[v].length; s++) {
				zemljevid[v][s] = sc.nextInt();
				
				if (zemljevid[v][s] > stDrzav) {
					stDrzav = zemljevid[v][s];
				}
				
				if (zemljevid[v][s] == 0) {
					stCelicMorja++;				
				}
			}
		}
		
		int[] dolzineObal = new int[stDrzav];
		boolean[][] sosedneDrzave = new boolean[stDrzav][stDrzav];
		
		for (int v = 0; v < zemljevid.length; v++) {
			for (int s = 0; s < zemljevid[v].length; s++) {
				if (zemljevid[v][s] != 0) {
					try {
						if (zemljevid[v - 1][s] == 0 || zemljevid[v][s - 1] == 0 || zemljevid[v][s + 1] == 0 || zemljevid[v + 1][s] == 0) {
							dolzineObal[zemljevid[v][s] - 1]++;
						}
					}catch (java.lang.ArrayIndexOutOfBoundsException el) {
						dolzineObal[zemljevid[v][s] - 1]++;
					}
					
					try {
						if (zemljevid[v][s + 1] != 0 && zemljevid[v][s + 1] != zemljevid[v][s]) {
							sosedneDrzave[zemljevid[v][s + 1] - 1][zemljevid[v][s] - 1] = true;
							sosedneDrzave[zemljevid[v][s] - 1][zemljevid[v][s + 1] - 1] = true;
						}
					}catch (java.lang.ArrayIndexOutOfBoundsException el){}	
					
					try {
						if (zemljevid[v + 1][s] != 0 && zemljevid[v + 1][s] != zemljevid[v][s]) {
							sosedneDrzave[zemljevid[v][s] - 1][zemljevid[v + 1][s] - 1] = true;
							sosedneDrzave[zemljevid[v + 1][s] - 1][zemljevid[v][s] - 1] = true;
						}
					}catch (java.lang.ArrayIndexOutOfBoundsException el){}	
				}	
			}
		}		
		
		int iMax = 0;
		int maxSosedi = 0;
		
		switch(sc.nextInt()) {
			case 1:
				System.out.println(stDrzav);
				break;
			case 2:
				System.out.println(stCelicMorja);
				break;
			case 3:
				for (int i = 0; i < dolzineObal.length; i++) {
					System.out.println(dolzineObal[i]);
				}
				break;
			case 4:
				for (int k = 0; k < stDrzav; k++) {
					sosedneDrzave[k][k] = false;
				}
				
				for (int i = 0; i < sosedneDrzave.length; i++) {
					int sosedi = 0;
					
					for (int j = 0; j < sosedneDrzave.length; j++) {
						if (sosedneDrzave[i][j]) {
							sosedi++;
						}
					}
					
					if (sosedi > maxSosedi) {
						maxSosedi = sosedi;
						iMax = i;
					}
					
				}
				
				System.out.println(iMax + 1);
				System.out.println(maxSosedi);
		}	
	}
}