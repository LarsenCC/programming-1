import java.util.Scanner;
import java.util.*;

public class DN09 {
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		ArrayList<String> text = new ArrayList<String>();
		ArrayList<Ukaz> ukazi = new ArrayList<Ukaz>();

		int vsota = 0;
		int i = 0;
		
		while (sc.hasNext()) {
			String ukaz = sc.next();
			
			switch (ukaz) {
				case "#": {
					String beseda = sc.next();
					Dodaj d = new Dodaj(text, beseda, -1);
					vsota += d.racun(text, beseda);
					ukazi.add(i, d);
					i++;
					break;
				}
				case "+": {
					int index = sc.nextInt();
					String beseda = sc.next();
					Dodaj d = new Dodaj(text, beseda, index);
					vsota += d.racun(text, beseda);
					ukazi.add(i, d);
					i++;
					break;
				}
				case "-": {
					int index = sc.nextInt();
					String beseda = text.get(index);
					Odstrani o = new Odstrani(text, beseda, index);
					vsota += o.racun(text, beseda);
					ukazi.add(i, o);
					i++;
					break;
				}
				case "<": {
					i--;
					if (text.size() > ukazi.get(i).getText().size()) {
						int dolzina = ukazi.get(i).getIndex() == -1 ? 0: 
						ukazi.get(i).getText().size() - ukazi.get(i).getIndex();
						
						vsota += odstraniCena(dolzina, ukazi.get(i).getBeseda().length());
					}else {
						int dolzina = ukazi.get(i).getIndex() == -1 ? 0 :
						ukazi.get(i).getText().size() - ukazi.get(i).getIndex() - 1;
						
						vsota += dodajCena(dolzina, ukazi.get(i).getBeseda().length());
					}
					text = ukazi.get(i).getText();
					break;
				}	
				case ">": {
					i++;
					if (text.size() < ukazi.get(i).getText().size()) {
						int dolzina = ukazi.get(i - 1).getIndex() == -1 ? 0 :
						ukazi.get(i).getText().size() - ukazi.get(i - 1).getIndex() - 1;
						
						vsota += dodajCena(dolzina, ukazi.get(i - 1).getBeseda().length());
					}else {
						int dolzina = ukazi.get(i - 1).getIndex() == -1 ? 0: 
						ukazi.get(i).getText().size() - ukazi.get(i - 1).getIndex();
						
						vsota += odstraniCena(dolzina, ukazi.get(i - 1).getBeseda().length());
					}
					text = ukazi.get(i).getText();
					break;
				}
				default:
					System.exit(0);
			}
			print(text, vsota);
		}
		
	}
	
	//metoda za izpis trenutnega ArrayLista
	public static void print(ArrayList<String> text, int denar) {
		System.out.printf("%d | ", denar);
		
		for (int i = 0; i < text.size(); i++) {
			if (i != 0 && i != text.size()) {
				System.out.print("/");
			}
			
			System.out.print(text.get(i));	
		}
		
		System.out.println();
	}
	
	//pomozne metoda, da ne rabm novih objektov delat
	public static int odstraniCena(int dolzina, int size) {
		return (3 * dolzina) + (2 * size);
	}
	
	public static int dodajCena(int dolzina, int size) {
		return 2 * dolzina + size;
	}
	
}

//abstrakten razred ki definira ostale
abstract class Ukaz {
	
	//vse je final da po tem ko se enkrat konstruira objek, teh vrednosti ne spreminjamo!
	private final int index;
	private final String beseda;
	private final ArrayList<String> text; // = new ArrayList<String>();
	
	public Ukaz (ArrayList<String> s, String beseda, int index) {
		this.text = new ArrayList<String>(s); // das s ki ga potem kopira!
		this.beseda = beseda;
		this.index = index;
	}
	
	public abstract int racun(ArrayList<String> s, String beseda);
	public abstract void izvedi(ArrayList<String> s, String beseda, int index);
	
	public int getIndex() {
		return this.index;
	}
	
	public String getBeseda() {
		return this.beseda;
	}
	
	public ArrayList<String> getText() {
		return this.text;
	}
	
}

//razred ki doda neki besedno na doloceno mesto
class Dodaj extends Ukaz {
	
	public Dodaj(ArrayList<String> s, String beseda, int index) {
		super(s, beseda, index);
		izvedi(s, beseda, index);
	}
	
	@Override
	public int racun(ArrayList<String> s, String beseda) {
		if (this.getIndex() == -1) {return beseda.length();}
		return 2 * (s.size() - this.getIndex() - 1) + beseda.length();
	}
	
	@Override
	public void izvedi(ArrayList<String> s, String beseda, int index) {
		if (index == -1) {
			s.add(beseda);
		}else {
			s.add(index, beseda);
		}
	}
	
}

//razred ki odstrani besedo iz dolocenega mesta
class Odstrani extends Ukaz {
	
	public Odstrani(ArrayList<String> s, String beseda, int index) {
		super(s, beseda, index);
		izvedi(s, beseda, index);
	}
	
	@Override
	public int racun(ArrayList<String> s, String beseda) {
		return 3 * (s.size() - this.getIndex()) + 2 * beseda.length();
	}
	
	@Override
	public void izvedi(ArrayList<String> s, String beseda, int index) {
		s.remove(index);
	}
	
}