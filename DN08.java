
public class DN08 {

    public static class Predmet {
        private int sifra;
        private String naziv;
        private int kt;
        private int tip;
        private Modul modul;
        private Izvajalec[] izvajalci;

        public Predmet(int sifra, String naziv, int kt, int tip, Modul modul, Izvajalec[] izvajalci) {
            this.sifra = sifra;
            this.naziv = naziv;
            this.kt = kt;
            this.tip = tip;
            this.modul = modul;
            this.izvajalci = izvajalci;
        }

        @Override
        public String toString() {
            return String.format("%d (%s)", this.sifra, this.naziv);
        }
    }
	
    public static class Modul {
        private int sifra;
        private String naziv;

        public Modul(int sifra, String naziv) {
            this.sifra = sifra;
            this.naziv = naziv;
        }

        @Override
        public String toString() {
            return String.format("%d (%s)", this.sifra, this.naziv);
        }
    }

    public static class Izvajalec {
        private int sifra;
        private String ip;

        public Izvajalec(int sifra, String ip) {
            this.sifra = sifra;
            this.ip = ip;
        }

        @Override
        public String toString() {
            return String.format("%d (%s)", this.sifra, this.ip);
        }
    }
	
    public static class Predmetnik {
        private Predmet[] predmeti;

        public Predmetnik(Predmet[] predmeti) {
            this.predmeti = predmeti;
        }

        public int steviloKT() {
			int sum = 0;
			
            for (int i = 0; i < predmeti.length; i++) {
				sum += predmeti[i].kt;
			}
			
            return sum;
        }

        public int[] tipiPredmetov() {
            int[] stTipov = new int[3];
			
			for (int i = 0; i < predmeti.length; i++) {
				stTipov[predmeti[i].tip]++;				
			}
			
            return stTipov;
        }

        public Predmet predmetZNajvecIzvajalci() {
			int max = 0;
			int imax = 0;
			
			if (predmeti.length == 0) {
				return null;
			}
			
            for (int i = 0; i < predmeti.length; i++) {
				if (max < predmeti[i].izvajalci.length) {
					max = predmeti[i].izvajalci.length;	
					imax = i;
				}else if (max == predmeti[i].izvajalci.length) {
					if (predmeti[i].sifra < predmeti[imax].sifra) {
						max = predmeti[i].izvajalci.length;	
						imax = i;
					}
				}
			}
			
            return predmeti[imax];
        }

        public int predmetiModula(int sifra, String[] nazivi) {
            int stNazivov = 0;
			
			for (int i = 0; i < predmeti.length; i++) {
				if (predmeti[i].modul != null && sifra == predmeti[i].modul.sifra) {
					nazivi[stNazivov] = predmeti[i].naziv;
					stNazivov++;
				}
			}
			
            return stNazivov;
        }

        public int steviloIzvajalcev() {
            int stMoznih = 0;
			int stIzvajalcev = 0;
			
			for (int i = 0; i < predmeti.length; i++) {
				stMoznih += predmeti[i].izvajalci.length;
			}
			
			Izvajalec[] iz = new Izvajalec[stMoznih];
			
			for (int i = 0; i < predmeti.length; i++) {
				for (int j = 0; j < predmeti[i].izvajalci.length; j++) {
					if (jeDrugacen(predmeti[i].izvajalci[j], iz)) {
						iz[stIzvajalcev] = predmeti[i].izvajalci[j];
						stIzvajalcev++;							
					}
				}
			}
			
            return stIzvajalcev;
        }

        public int steviloEnakoMocnihModulov(Predmetnik drugi) {
			int stMod = 0;
			int st = 0;
			Predmet[] d, k;
			
			if (this.predmeti.length < drugi.predmeti.length) {
				d = drugi.predmeti;
				k = this.predmeti;
			}else {
				d = this.predmeti;
				k = drugi.predmeti;
			}
			
			int[] moduli = new int[k.length];
			
			for (int i = 0; i < k.length; i++) {
				if (k[i].tip == 2) {
					if (jeDrugacen(k[i].modul.sifra, moduli)) {
						moduli[stMod] = k[i].modul.sifra;
						stMod++;
					}
				}
			}
			
			int stP1 = 0;
			int stP2 = 0;
			
			for (int i = 0; i < stMod; i++) {
				for (int a = 0; a < k.length; a++) {
					if (k[a].tip == 2 && k[a].modul.sifra == moduli[i]) {
						stP1++;
					}
				}
				
				for (int b = 0; b < d.length; b++) {
					if (d[b].tip == 2 && d[b].modul.sifra == moduli[i]) {
						stP2++;
					}
				}
				
				if (stP1 == stP2) {
					st++;
				}
				
				stP1 = 0;
				stP2 = 0;
			}
			
            return st;
        }
		
		//lastna metoda ...
		private boolean jeDrugacen (Izvajalec iz, Izvajalec[] tabela) {
			for (int i = 0; i < tabela.length; i++) {
				if (tabela[i] == null) {
					break;
				}
				
				//pejt mogoce po sifrah!!!
				if (tabela[i].equals(iz)) {
					return false;
				}
			}
			
			return true;
		}
		
		//lastna metoda
		private boolean jeDrugacen (int sifra, int[] tabela) {
			for (int i = 0; i < tabela.length; i++) {
				if (tabela[i] == 0) {
					break;
				}
				
				//pejt mogoce po sifrah!!!
				if (tabela[i] == sifra) {
					return false;
				}
			}
			
			return true;
		}
		
    }
}
